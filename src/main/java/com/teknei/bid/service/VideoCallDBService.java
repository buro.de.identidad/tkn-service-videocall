package com.teknei.bid.service;

import com.teknei.bid.persistence.entities.BidVide;
import com.teknei.bid.persistence.repository.BidVideRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
public class VideoCallDBService {

    @Autowired
    private BidVideRepository bidVideRepository;

    public void addRecordToDB(Long idCustomer, String currentUser) {
        BidVide bidVide = new BidVide();
        bidVide.setFchCrea(new Timestamp(System.currentTimeMillis()));
        bidVide.setIdClie(idCustomer);
        bidVide.setIdEsta(1);
        bidVide.setIdTipo(3);
        bidVide.setIdTasVide(idCustomer);
        bidVide.setUsrCrea(currentUser);
        bidVide.setUsrOpeCrea(currentUser);//MCG
        bidVideRepository.save(bidVide);
    }

}
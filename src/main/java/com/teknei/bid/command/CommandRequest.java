package com.teknei.bid.command;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.util.List;

@Data
public class CommandRequest implements Serializable{

    private List<MultipartFile> files;
    private Long id;
    private String scanId;
    private String documentId;
    private RequestType requestType;
    private String data;
    private String curp;
    private String data2;
    private byte[] selfie;
    private byte[] caller;
    private byte[] callee;
    private Status requestStatus;
    private String userOpeCrea; //MCG Se añade porque si no, da error al activarse el trigger de auditoría por estar a null
}